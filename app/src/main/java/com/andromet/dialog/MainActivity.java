package com.andromet.dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText editTextUser, editTextPassword;
    String strUserName, strPassword;
    CheckBox checkBoxSave;
    View layout;
    AlertDialog.Builder alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void onDialogClicked(View view) {

        layout = getLayoutInflater().inflate(R.layout.custom_dialog, null);
        editTextUser = layout.findViewById(R.id.editTextUserName);
        editTextPassword = layout.findViewById(R.id.editTextPassword);
        checkBoxSave = layout.findViewById(R.id.checkboxSave);

        checkBoxSave.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    editTextPassword.setTransformationMethod(null);
                } else {
                    editTextPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
            }
        });

        alert = new AlertDialog.Builder(this);
        alert.setTitle("Sign Up");
        alert.setView(layout);
        alert.setCancelable(false);

        alert.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                strUserName = editTextUser.getText().toString();
                strPassword = editTextPassword.getText().toString();

                Toast.makeText(MainActivity.this, "Name: " + strUserName +
                        " Password: " + strPassword, Toast.LENGTH_SHORT).show();
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Toast.makeText(MainActivity.this, "You've pressed negative button",
                        Toast.LENGTH_SHORT).show();
            }
        });

        AlertDialog dialog = alert.create();
        dialog.show();
    }
}
